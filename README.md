# Backend Datastreamers

Backend bude poměrně jednoduchý.

![Datastreamers backend](./docs/datastreamers_flow.png "Datastreamers backend")

  * senzor bude realizován na bázi esp32
  * data budou přenášena pomocí protokolu MQTT pomocí lokální WiFi sítě
  * server s backendem bude realizovat
    * MQTT Broker
    * InfuxDB pro ukládání dat
    * Grafana pro vizualizaci dat

## Instalace serveru

Připravím image pro VirtualBox, ale stack lze nasadit i pomocí Ansible skriptu. Prerekvizita je minimální instalace CentOS 7 a ssh přístup (bude pravděpodobně nutné editovat soubor hosts v ansible adresáři)

```
cd data_streamers_backend/ansible/data_streamers_backend
ansible-playbook -i hosts site.yml
```

## Konfigurace InfluxDB

Po instalaci je potřeba připravit Infux databázi (mohl bych to realizovat též pomocí Ansible, TODO). Influx konzole se spustí pomocí

```
influx
```

```
create user datastreamers with password '********' with all privileges
create database datastreamers
```

## Konfigurace Grafana

Grafana poslouchá na ip adrese serveru a portu 3000.

<http://ip_address:3000>

Po prvním přihlášení (admin/admin) je vynucena změna hesla.

Dále je potřeba nakonfigurovat datasource InfluxDB:

  * URL: http://localhost:8086
  * Database: datastreamers
  * User: datastreamers
  * Password: ********

Ostatní nastavení lze nechat defaultní

## Testování

### MQTT

Utilitka mqtt-forwarder na serveru dělá jednoduchou věc - subscribne si topic `datastreamers/#` a čeká, co kdo do něj pošle. Pokud má zpráva topic ve správném formátu (`datastreamers/<dev_id>/<measurement>`), tak se naměřená hodnota vloží do InfluxDB.

Testovací odeslání mqtt zprávy se dá udělat takto:

```
mosquitto_pub -h <ip_address> -t "datastreamers/teplomer_1/teplota" -m '22.9' -d
```

debug výstup

```
Client mosq-6SWvDgXyLTq5EqhjT2 sending CONNECT
Client mosq-6SWvDgXyLTq5EqhjT2 received CONNACK (0)
Client mosq-6SWvDgXyLTq5EqhjT2 sending PUBLISH (d0, q0, r0, m1, 'datastreamers/teplomer_1/teplota', ... (4 bytes))
Client mosq-6SWvDgXyLTq5EqhjT2 sending DISCONNECT
```

V logu na serveru by mělo být vidět propsání hodnoty do InfluxDB

```
tail /var/log/messages
```

```
Jan 11 21:19:18 datastreamer influxd: [httpd] ::1 - - [11/Jan/2020:21:19:18 +0100] "POST /query?chunked=true&db=datastreamers&epoch=ns&q=SHOW+measurements HTTP/1.1" 200 110 "-" "InfluxDBShell/1.7.9" a57158cd-34af-11ea-803b-08002746b5c9 908
```

### Grafana

V Grafaně je potřeba si nadefinovat nějaký dashboard a query v něm nadefinovat zhruba takto:

![Grafana query](./docs/grafana_query.png "Grafana query")
